# Introduction

The Micro Services on this projects is the same app that was used in the Lecture in DevOps Bootcamp.
This project follows the objective for The capstone Project 4.

<img width="650" alt="petclinic-screenshot" src="https://lh5.googleusercontent.com/vfX3Fe1WO5P58KclMPpAqo89FVzpPuGUVbp8t9673oZgwLfXO5CMQyuScsDXToKNNULXs1S8cZxbN_ulLFYowd0IR7qhRESUfih5gH5zAj4uWLJemRyGi_G-JBVOLUSpsw=w386">

I could not register in Linode successfully and contacting their support just goes to nowhere, so EKS will have to do for this project. The EKS is provision by terraform which is included in this project.

Bootcamp Chapter 4 Git Repo:
- git@gitlab.com:twwnana/kubernetes/oneline-shop-microservices.git

# Creating the EKS Cluster

Navigate to the terraform folder of the project and issue the command below. Wait for it to finish provisioning the EKS Cluster.

```
cd terraform
terraform init -migrate-state
terraform apply -var-file custom-variables/vpc-variables.tfvars -auto-approve
```
# Requirements before deploying the Micro Services Apps
The endpoint executing the kubectl command must have the aws cli tool installed and the "aws config" set up.

# Deploying the Micro Services Apps

1. Connect to the K8s cluster context
    ```
    aws eks update-kubeconfig --name micro_svc_k8s_cluster
    ```

    NOTE: You might get a argument is deprecated in the output similar below.
    ```
    ...
    module.eks.aws_eks_addon.this["aws-ebs-csi-driver"]: Still creating... [1m0s elapsed]
    module.eks.aws_eks_addon.this["vpc-cni"]: Still creating... [1m0s elapsed]
    module.eks.aws_eks_addon.this["vpc-cni"]: Creation complete after 1m5s [id=micro_svc_k8s_cluster:vpc-cni]
    module.eks.aws_eks_addon.this["aws-ebs-csi-driver"]: Still creating... [1m10s elapsed]
    module.eks.aws_eks_addon.this["aws-ebs-csi-driver"]: Creation complete after 1m16s [id=micro_svc_k8s_cluster:aws-ebs-csi-driver]
    ╷
    │ Warning: Argument is deprecated
    │ 
    │   with module.eks.aws_eks_addon.this["vpc-cni"],
    │   on .terraform/modules/eks/main.tf line 392, in resource "aws_eks_addon" "this":
    │  392:   resolve_conflicts        = try(each.value.resolve_conflicts, "OVERWRITE")
    │ 
    │ The "resolve_conflicts" attribute can't be set to "PRESERVE" on initial resource creation. Use "resolve_conflicts_on_create" and/or "resolve_conflicts_on_update" instead
    │ 
    │ (and 7 more similar warnings elsewhere)
    ╵

    Apply complete! Resources: 62 added, 0 changed, 0 destroyed.
    ```

2. Apply the manifest files for the micro services.

    NOTE: This will deploy the micro services to the cluster.

    ```
    cd k8s-app
    kubectl apply -f config.yaml

    deployment.apps/emailservice created
    service/emailservice created
    deployment.apps/recommendationservice created
    service/recommendationservice created
    deployment.apps/productcatalogservice created
    service/productcatalogservice created
    deployment.apps/paymentservice created
    service/paymentservice created
    deployment.apps/currencyservice created
    service/currencyservice created
    deployment.apps/shippingservice created
    service/shippingservice created
    deployment.apps/adservice created
    service/adservice created
    deployment.apps/cartservice created
    service/cartservice created
    deployment.apps/redis-cart created
    service/redis-cart created
    deployment.apps/checkoutservice created
    service/checkoutservice created
    deployment.apps/frontendservice created
    service/frontendservice created
    ```

3. Check if all the pods are created and is in the running state as well as the services.
    ```
    kubectl get pods

    NAME                                     READY   STATUS    RESTARTS   AGE
    adservice-7bf4556694-r6lxx               1/1     Running   0          25s
    cartservice-7867d9985b-9d5t2             1/1     Running   0          25s
    checkoutservice-594655ffdc-wf8pk         1/1     Running   0          25s
    currencyservice-7d78d979b8-g4r2k         1/1     Running   0          26s
    emailservice-7ff444889d-dtnrj            1/1     Running   0          27s
    frontendservice-6b869f7488-nbpjx         1/1     Running   0          24s
    paymentservice-6599f8b49f-7zfnq          1/1     Running   0          26s
    productcatalogservice-9bb75888f-8v99w    1/1     Running   0          26s
    recommendationservice-5f75959bbb-xbvbv   1/1     Running   0          26s
    redis-cart-77f675d75b-9d5zk              1/1     Running   0          25s
    shippingservice-b6557cd75-4l5c9          1/1     Running   0          26s

    kubectl get svc
    NAME                    TYPE           CLUSTER-IP       EXTERNAL-IP                                                                    PORT(S)        AGE
    adservice               ClusterIP      172.20.107.136   <none>                                                                         9555/TCP       90s
    cartservice             ClusterIP      172.20.25.76     <none>                                                                         7070/TCP       90s
    checkoutservice         ClusterIP      172.20.59.133    <none>                                                                         5050/TCP       90s
    currencyservice         ClusterIP      172.20.105.57    <none>                                                                         7000/TCP       91s
    emailservice            ClusterIP      172.20.49.237    <none>                                                                         5000/TCP       92s
    frontendservice         LoadBalancer   172.20.129.244   a6a3127356d2141b882a2b0f83363722-1985094388.ap-southeast-1.elb.amazonaws.com   80:32680/TCP   89s
    kubernetes              ClusterIP      172.20.0.1       <none>                                                                         443/TCP        10m
    paymentservice          ClusterIP      172.20.216.95    <none>                                                                         50051/TCP      91s
    productcatalogservice   ClusterIP      172.20.28.41     <none>                                                                         3550/TCP       91s
    recommendationservice   ClusterIP      172.20.222.202   <none>                                                                         8080/TCP       91s
    redis-cart              ClusterIP      172.20.28.253    <none>                                                                         6379/TCP       90s
    shippingservice         ClusterIP      172.20.114.160   <none>                                                                         50051/TCP      91s
    ```

# Testing the application Output
Copy the External-IP for the frontendservice and paste it on the browser to test the app.

NOTE: The EXTERNAL-IP might be different in your environment.