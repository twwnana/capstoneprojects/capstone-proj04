terraform {
  required_version = ">= 1.6.1"
  backend "s3" {
    bucket = "twwn-capstore-proj"
    key    = "state-capstone-proj04.tfstate"
    region = "ap-southeast-1"
  }
}
